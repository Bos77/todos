import axios, { AxiosError } from "axios";

import { appConfig } from "config";

import { IHttpClient } from "shared/types";

const axiosInstance = axios.create({
	baseURL: appConfig.baseUrl,
	withCredentials: true,
});

export const httpClient: IHttpClient = {
	get: (url) => axiosInstance.get(url).then((res) => res.data),
	post: (url, body) => axiosInstance.post(url, body).then((res) => res.data),
	put: (url, body) => axiosInstance.put(url, body).then((res) => res.data),
	delete: (url) => axiosInstance.delete(url).then((res) => res.data),
};

export const initHttpClient = (errorInterceptor: (error: AxiosError) => void) => {
	axiosInstance.interceptors.response.use(
		function (response) {
			return response;
		},
		function (error) {
			errorInterceptor(error);
			return Promise.reject(error);
		}
	);
};
