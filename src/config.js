export const appConfig = {
	baseUrl: process.env.REACT_APP_API_ROOT,
	lngCode: "uz",
	theme: "light",
};
