import { get, isArray } from "lodash";

import { Model } from "./model";

export const mapSignInResponse = (data: unknown): Model.IEntity => ({
	name: get(data, "name") ?? "",
	role: get(data, "role") ?? "",
});

export const mapUserListResponse = (data: unknown): Model.IEntity[] => {
	if (isArray(data)) {
		return data.map((user) => ({
			name: get(user, "name") ?? "",
			role: get(user, "role") ?? "",
		}));
	}

	return [];
};
