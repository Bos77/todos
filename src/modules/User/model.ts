export namespace Model {
	export interface IEntity {
		name: string;
		role: string;
	}

	export interface ISignInForm {
		login: string;
		password: string;
	}

	export const Roles = {
		admin: "admin",
		user: "user",
	};

	export type Role = "admin" | "user";

	export const ME = "@@USER/ME";
	export const LIST = "@@USER/LIST";
}

export interface IRepository {
	signIn: (credentials: Model.ISignInForm) => Promise<Model.IEntity>;
	signOut: () => Promise<void>;
	getCurrentCredentials: () => Promise<Model.IEntity>;
	getUsers: () => Promise<Model.IEntity[]>;
}
