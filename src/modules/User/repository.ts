import { IHttpClient } from "../../shared/types";

import { IRepository, Model } from "./model";

export class RepositoryImpl implements IRepository {
	constructor(private readonly httpClient: IHttpClient) {}

	signIn(credentials: Model.ISignInForm): Promise<Model.IEntity> {
		return this.httpClient.post("/login", credentials);
	}

	signOut(): Promise<void> {
		return this.httpClient.post("/logout");
	}

	getCurrentCredentials(): Promise<Model.IEntity> {
		return this.httpClient.get("/me");
	}

	getUsers(): Promise<Model.IEntity[]> {
		return this.httpClient.get("/users");
	}
}
