import { UserModel } from "../User";

export namespace Model {
	export interface IEntity {
		id: UniqueId;
		title: string;
		description: string;
		createdBy: UserModel.Role;
	}

	export interface ITodoForm {
		title: string;
		description: string;
	}

	export const LIST = "@@TODO/LIST";
	export const SINGLE = "@@TODO/SINGLE";
}

export interface IRepository {
	getTodos: () => Promise<Model.IEntity>;
	getTodo: (id: number) => Promise<Model.IEntity>;
	createTodo: (body: Model.ITodoForm) => Promise<Model.IEntity>;
	updateTodo: (id: number, body: Model.ITodoForm) => Promise<void>;
	deleteTodo: (id: number) => Promise<void>;
}
