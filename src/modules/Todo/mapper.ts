import { get, isArray } from "lodash";

import { Model } from "./model";

export const mapTodosListResponse = (todos: unknown): Model.IEntity[] => {
	if (isArray(todos)) {
		return todos.map((todo) => ({
			id: get(todo, "id"),
			title: get(todo, "title") ?? "",
			description: get(todo, "description") ?? "",
			createdBy: get(todo, "createdBy") ?? "",
		}));
	}

	return [];
};

export const mapTodoResponse = (todo: unknown): Model.ITodoForm => {
	return {
		title: get(todo, "title") ?? "",
		description: get(todo, "description") ?? "",
	};
};
