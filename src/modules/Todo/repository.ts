import { IHttpClient } from "../../shared/types";

import { IRepository, Model } from "./model";

export class RepositoryImpl implements IRepository {
	constructor(private readonly httpClient: IHttpClient) {}

	getTodos(): Promise<Model.IEntity> {
		return this.httpClient.get("/todos");
	}

	getTodo(id: number): Promise<Model.IEntity> {
		return this.httpClient.get(`/todos/${id}`);
	}

	createTodo(todo: Model.ITodoForm): Promise<Model.IEntity> {
		return this.httpClient.post("todos", todo);
	}

	updateTodo(id: number, todo: Model.ITodoForm): Promise<void> {
		return this.httpClient.put(`/todos/${id}`, todo);
	}

	deleteTodo(id: number): Promise<void> {
		return this.httpClient.delete(`/todos/${id}`);
	}
}
