export interface IHttpClient {
	get: <Response>(url: string) => Promise<Response>;
	post: <Response>(url: string, body?: any) => Promise<Response>;
	put: <Response>(url: string, body?: any) => Promise<Response>;
	delete: <Response>(url: string) => Promise<Response>;
}
