import { useEffect, useRef } from "react";

export const useOutsideClick = (handler: (event: MouseEvent) => void) => {
	const ref = useRef<HTMLElement>(null);

	useEffect(() => {
		const handleClickOutside = (event: MouseEvent) => {
			if (ref.current && !ref.current.contains(event.target as Node)) {
				handler(event);
			}
		};

		document.addEventListener("click", handleClickOutside);
		return () => {
			document.removeEventListener("click", handleClickOutside);
		};
	}, [ref, handler]);

	return ref;
};
