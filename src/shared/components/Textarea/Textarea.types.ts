import { TextareaHTMLAttributes } from "react";

export interface TextareaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
	variant: "outlined";
	outerClass?: string;
	innerClass?: string;
	label?: string;
	errorMessage?: string;
	isDisabled?: boolean;
}
