import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Textarea } from "./Textarea";

export default {
	name: "Components/Input",
	component: Textarea,
} as ComponentMeta<typeof Textarea>;

const Template: ComponentStory<typeof Textarea> = (args) => <Textarea {...args} />;

export const TextareaDefault = Template.bind({});
TextareaDefault.args = {
	label: "Description",
	placeholder: "Write some notes",
	variant: "outlined",
};

export const TextareaError = Template.bind({});
TextareaError.args = {
	label: "Description",
	placeholder: "Write some notes",
	variant: "outlined",
	errorMessage: "Required",
};
