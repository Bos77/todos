import React, { forwardRef } from "react";
import cn from "classnames";

import { ControlError, ControlLabel } from "../Control";

import { TextareaProps } from "./Textarea.types";

import "./Textarea.scss";

export const Textarea = forwardRef<HTMLTextAreaElement, TextareaProps>(
	(
		{
			variant,
			label,
			errorMessage,
			outerClass = "",
			innerClass = "",
			className = "",
			isDisabled = false,
			...restProps
		},
		ref
	) => {
		return (
			<div className={cn("control", outerClass)}>
				<ControlLabel label={label} />

				<div className={cn("input__container", innerClass)}>
					<textarea
						ref={ref}
						disabled={isDisabled}
						className={cn(
							"input__field",
							`input__field_${variant}`,
							"textarea",
							className,
							{
								input__field_disabled: isDisabled,
								input__field_error: errorMessage,
							}
						)}
						{...restProps}
					/>
				</div>

				<ControlError errorMessage={errorMessage} />
			</div>
		);
	}
);
