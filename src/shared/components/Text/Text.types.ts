import React from "react";

export type TextTags = "p" | "span";

export type TextProps = {
	as: TextTags;
	text: string;
	weight: "extra-bold" | "bold" | "semi-bold" | "regular";
	size: "x-lg" | "lg" | "md" | "sm" | "x-sm";
} & React.ComponentPropsWithoutRef<TextTags>;
