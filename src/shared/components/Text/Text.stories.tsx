import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Text } from "./Text";

export default {
	name: "Components/Text",
	component: Text,
} as ComponentMeta<typeof Text>;

const Template: ComponentStory<typeof Text> = (args) => <Text {...args} />;

export const TextXLg = Template.bind({});
TextXLg.args = {
	as: "p",
	text: "Text",
	size: "x-lg",
};

export const TextLg = Template.bind({});
TextLg.args = {
	as: "p",
	text: "Text",
	size: "lg",
};

export const TextMd = Template.bind({});
TextMd.args = {
	as: "p",
	text: "Text",
	size: "md",
};

export const TextSm = Template.bind({});
TextSm.args = {
	as: "p",
	text: "Text",
	size: "sm",
};

export const TextXSm = Template.bind({});
TextXSm.args = {
	as: "p",
	text: "Text",
	size: "x-sm",
};
