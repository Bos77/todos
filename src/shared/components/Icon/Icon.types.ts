import { SVGProps } from "react";

export interface IconProps extends SVGProps<SVGElement> {
	icon: IconNames;
}

export type IconType = Record<IconNames, any>;

export type IconNames =
	| "filter"
	| "logo"
	| "dashboard"
	| "arrow-down"
	| "sign-in-hero"
	| "logout"
	| "add-plus"
	| "edit"
	| "delete"
	| "users"
	| "overlay-close"
	| "no-data"
	| "error";
