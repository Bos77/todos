import React from "react";

import { IconProps, IconType } from "./Icon.types";

import { ReactComponent as AddPlus } from "assets/icons/add-plus.svg";
import { ReactComponent as ArrowDown } from "assets/icons/arrow-down.svg";
import { ReactComponent as Dashboard } from "assets/icons/dashboard.svg";
import { ReactComponent as Delete } from "assets/icons/delete.svg";
import { ReactComponent as Edit } from "assets/icons/edit.svg";
import { ReactComponent as Error } from "assets/icons/error.svg";
import { ReactComponent as Filter } from "assets/icons/filter.svg";
import { ReactComponent as Logout } from "assets/icons/log-out.svg";
import { ReactComponent as Logo } from "assets/icons/logo.svg";
import { ReactComponent as NoData } from "assets/icons/no-data.svg";
import { ReactComponent as OverlayClose } from "assets/icons/overlay-close.svg";
import { ReactComponent as SignInHero } from "assets/icons/sign-in-bg.svg";
import { ReactComponent as Users } from "assets/icons/users.svg";

const icons: IconType = {
	filter: Filter,
	logo: Logo,
	dashboard: Dashboard,
	logout: Logout,
	"arrow-down": ArrowDown,
	"sign-in-hero": SignInHero,
	"add-plus": AddPlus,
	edit: Edit,
	delete: Delete,
	users: Users,
	"overlay-close": OverlayClose,
	"no-data": NoData,
	error: Error,
};

export const Icon: React.FC<IconProps> = ({ icon, ...restProps }) => {
	const SVG = icons[icon];

	return <SVG {...restProps} />;
};
