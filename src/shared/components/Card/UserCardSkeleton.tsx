import React from "react";
import Skeleton from "react-loading-skeleton";

export const UserCardSkeleton = () => {
	return (
		<div className="user-card">
			<Skeleton circle={true} width="50px" height="50px" className="mb-6" />
			<div>
				<Skeleton count={1} width="120px" height="25px" className="mb-2" />
				<Skeleton count={1} width="80px" height="25px" className="mx-auto block" />
			</div>
		</div>
	);
};
