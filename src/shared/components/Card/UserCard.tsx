import React from "react";

import { Avatar } from "../Avatar";
import { Text } from "../Text";

import { UserCardProps } from "./UserCard.types";

import "./UserCard.scss";

export const UserCard: React.FC<UserCardProps> = ({ role, name }) => {
	return (
		<div className="user-card">
			<Avatar size="lg" className="mb-6" />
			<Text as="p" text={name} weight="bold" size="md" className="mb-2" />
			<Text as="p" text={role} weight="regular" size="md" />
		</div>
	);
};
