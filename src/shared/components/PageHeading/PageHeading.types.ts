import React from "react";

export interface PageHeadingProps {
	title: string;
	btnText?: string;
	onBtnClick?: React.MouseEventHandler<HTMLButtonElement>;
}
