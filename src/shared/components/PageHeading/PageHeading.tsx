import React from "react";

import { Button } from "../Button";
import { Icon } from "../Icon";
import { Title } from "../Title";

import { PageHeadingProps } from "./PageHeading.types";

export const PageHeading: React.FC<PageHeadingProps> = ({ title, btnText, onBtnClick }) => {
	return (
		<div className="mb-10 flex items-center justify-between">
			<Title as="h2" text={title} />

			{(btnText || onBtnClick) && (
				<Button variant="primary" size="lg" onClick={onBtnClick}>
					<Icon icon="add-plus" className="mr-2" />

					{btnText}
				</Button>
			)}
		</div>
	);
};
