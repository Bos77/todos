import React from "react";
import cn from "classnames";

import { ButtonProps } from "./Button.types";

import "./Button.scss";

export const Button: React.FC<ButtonProps> = ({
	text,
	isDisabled = false,
	isLoading = false,
	variant,
	size,
	className = "",
	roundness = "half",
	children,
	...restProps
}) => {
	const classNames = cn(
		"btn",
		`btn_${variant}`,
		`btn_${size}`,
		`radius_${roundness}`,
		{
			btn_disabled: isDisabled,
		},
		className
	);

	return (
		<button className={classNames} disabled={isDisabled || isLoading} {...restProps}>
			{text}
			{children}

			{isLoading && <span className="btn-spinner"></span>}
		</button>
	);
};
