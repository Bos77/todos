import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Button } from "./Button";

export default {
	name: "Components/Button",
	component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const PrimaryButton = Template.bind({});
PrimaryButton.args = {
	variant: "primary",
	size: "lg",
	text: "Click me",
};

export const OutlinedButton = Template.bind({});
OutlinedButton.args = {
	variant: "outlined",
	size: "lg",
	text: "Click me",
};

export const FilledButton = Template.bind({});
FilledButton.args = {
	variant: "filled",
	size: "lg",
	text: "Click me",
};
