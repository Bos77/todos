import React from "react";

export type ButtonProps = {
	variant: "primary" | "outlined" | "filled";
	size: "lg" | "md" | "sm" | "icon";
	text?: string;
	isDisabled?: boolean;
	isLoading?: boolean;
	roundness?: "half" | "full";
} & Omit<React.ComponentPropsWithoutRef<"button">, "disabled">;
