import React from "react";
import cn from "classnames";

import { TitleProps, TitleTags } from "./Title.types";

import "./Title.scss";

export const Title: React.FC<TitleProps> = ({ as, text, children, className, ...restProps }) => {
	const Component = as;

	const classNames = cn("title", `title_${mapTitleSize(as)}`, className);

	return (
		<Component className={classNames} {...restProps}>
			{children || text}
		</Component>
	);
};

const mapTitleSize = (tag: TitleTags) => {
	const size = tag.charAt(1);

	switch (size) {
		case "1":
			return "x-lg";
		case "2":
			return "lg";
		case "3":
			return "x-md";
		case "4":
			return "md";
		case "5":
			return "sm";
		case "6":
			return "x-sm";
		default:
			return "";
	}
};
