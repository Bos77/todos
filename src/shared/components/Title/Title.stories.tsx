import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Title } from "./index";

export default {
	name: "Components/Title",
	component: Title,
} as ComponentMeta<typeof Title>;

const Template: ComponentStory<typeof Title> = (args) => <Title {...args} />;

export const TitleXLg = Template.bind({});
TitleXLg.args = {
	as: "h1",
	text: "Title",
};

export const TitleLg = Template.bind({});
TitleLg.args = {
	as: "h2",
	text: "Title",
};

export const TitleXMd = Template.bind({});
TitleXMd.args = {
	as: "h3",
	text: "Title",
};

export const TitleMd = Template.bind({});
TitleMd.args = {
	as: "h4",
	text: "Title",
};

export const TitleSm = Template.bind({});
TitleSm.args = {
	as: "h5",
	text: "Title",
};

export const TitleXSm = Template.bind({});
TitleXSm.args = {
	as: "h6",
	text: "Title",
};
