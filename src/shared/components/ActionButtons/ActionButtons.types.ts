import React from "react";

export interface ActionButtonsProps {
	onEdit: React.MouseEventHandler;
	onDelete: React.MouseEventHandler;
}
