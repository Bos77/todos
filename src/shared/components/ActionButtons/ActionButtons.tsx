import React from "react";

import { Button } from "../Button";
import { Icon } from "../Icon";

import { ActionButtonsProps } from "./ActionButtons.types";

import "./ActionButtons.scss";

export const ActionButtons: React.FC<ActionButtonsProps> = ({ onEdit, onDelete }) => {
	return (
		<div className="table-buttons">
			<Button variant="filled" size="md" className="table-buttons__btn" onClick={onEdit}>
				<Icon icon="edit" />
			</Button>

			<Button variant="filled" size="md" className="table-buttons__btn" onClick={onDelete}>
				<Icon icon="delete" />
			</Button>
		</div>
	);
};
