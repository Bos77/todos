import React, { forwardRef } from "react";
import cn from "classnames";

import { ControlError, ControlLabel } from "../Control";

import { InputProps } from "./Input.types";

import "./Input.scss";

export const Input = forwardRef<HTMLInputElement, InputProps>(
	(
		{
			size,
			variant,
			label,
			errorMessage,
			outerClass = "",
			innerClass = "",
			className = "",
			isDisabled = false,
			...restProps
		},
		ref
	) => {
		return (
			<div className={cn("control", outerClass)}>
				<ControlLabel label={label} />

				<div className={cn("input__container", innerClass)}>
					<input
						ref={ref}
						disabled={isDisabled}
						className={cn(
							"input__field",
							`input__field_${variant}`,
							`input__field_${size}`,
							className,
							{
								input__field_disabled: isDisabled,
								input__field_error: errorMessage,
							}
						)}
						{...restProps}
					/>
				</div>

				<ControlError errorMessage={errorMessage} />
			</div>
		);
	}
);
