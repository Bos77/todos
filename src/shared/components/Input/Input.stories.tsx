import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Input } from "./Input";

export default {
	name: "Components/Input",
	component: Input,
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

export const InputDefault = Template.bind({});
InputDefault.args = {
	label: "Email",
	placeholder: "test@gmail.com",
	variant: "outlined",
	size: "lg",
};

export const InputError = Template.bind({});
InputError.args = {
	label: "Email",
	placeholder: "test@gmail.com",
	variant: "outlined",
	size: "lg",
	errorMessage: "Required",
};
