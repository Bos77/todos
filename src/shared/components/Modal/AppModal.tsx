import React from "react";

import { Button } from "../Button";
import { Icon } from "../Icon";
import { Title } from "../Title";

import { Modal } from "./Modal";
import { AppModalProps } from "./Modal.types";

export const AppModal: React.FC<AppModalProps> = ({ isOpen, title, onClose, children }) => {
	return (
		<Modal isOpen={isOpen} onClose={onClose}>
			<Button variant="filled" size="icon" onClick={onClose} className="modal__close">
				<Icon icon="overlay-close" />
			</Button>

			<Title as="h3" text={title} className="mb-8" />

			{children}
		</Modal>
	);
};
