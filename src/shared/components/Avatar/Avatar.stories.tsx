import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Avatar } from "./Avatar";

export default {
	name: "Components/Input",
	component: Avatar,
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

export const AvatarLg = Template.bind({});
AvatarLg.args = {
	size: "lg",
};

export const AvatarMd = Template.bind({});
AvatarMd.args = {
	size: "md",
};

export const AvatarSm = Template.bind({});
AvatarSm.args = {
	size: "sm",
};
