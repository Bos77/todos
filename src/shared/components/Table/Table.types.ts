import React from "react";

export interface TableProps<TItem> {
	rowKey: keyof TItem;
	items: TItem[];
	isLoading?: boolean;
	emptyUiText?: string;
	columns: Array<{
		title: string;
		dataKey: keyof TItem;
		render: (item: any, values: TItem) => React.ReactNode;
	}>;
}
