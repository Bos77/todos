import React from "react";

import { Icon } from "../Icon";
import { Text } from "../Text";

interface NoDataProps {
	noDataText?: string;
}

export const NoData: React.FC<NoDataProps> = ({ noDataText }) => {
	return (
		<div className="app-table__no-data">
			<Icon icon="no-data" className="margin" />
			{noDataText && (
				<Text
					as="p"
					text={noDataText}
					weight="bold"
					size="x-lg"
					className="mt-6 max-w-6xl text-center"
				/>
			)}
		</div>
	);
};
