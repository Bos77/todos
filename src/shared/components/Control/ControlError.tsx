import React from "react";

import { Text } from "../Text";

import { ControlErrorProps } from "./Control.types";

export const ControlError: React.FC<ControlErrorProps> = ({ errorMessage }) => {
	return errorMessage ? (
		<Text as="span" text={errorMessage} weight="bold" size="x-sm" className="control-error" />
	) : null;
};
