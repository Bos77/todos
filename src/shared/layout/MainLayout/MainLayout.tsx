import React from "react";
import { Outlet } from "react-router-dom";

import { UserModel } from "../../../modules/User";
import { useAuth } from "../../context";
import { Header } from "../Header";
import { Sidebar } from "../Sidebar";

import "./MainLayout.scss";

export const MainLayout = () => {
	const auth = useAuth();

	return (
		<>
			<Sidebar
				menu={
					auth?.user?.role === UserModel.Roles.admin
						? [
								{
									id: "todos",
									label: "Todos",
									link: "/",
									icon: "dashboard",
								},
								{
									id: "users",
									label: "Users",
									link: "/users",
									icon: "users",
								},
						  ]
						: [
								{
									id: "todos",
									label: "Todos",
									link: "/",
									icon: "dashboard",
								},
						  ]
				}
			/>

			<div className="content">
				<Header />

				<main className="main">
					<Outlet />
				</main>
			</div>
		</>
	);
};
