import React from "react";
import { NavLink } from "react-router-dom";

import { Icon } from "../../components";

import { SidebarProps } from "./Sidebar.types";

import "./Sidebar.scss";

export const Sidebar: React.FC<SidebarProps> = ({ menu }) => {
	return (
		<aside className="sidebar">
			<div className="mb-16 ml-7">
				<Icon icon="logo" />
			</div>

			<nav className="ml-7 mr-4">
				{menu.map((menuItem) => (
					<NavLink
						key={menuItem.id}
						to={menuItem.link}
						className={({ isActive }) =>
							isActive ? "sidebar__link sidebar__link_active" : "sidebar__link"
						}
					>
						<Icon icon={menuItem.icon} className="mr-5" />
						{menuItem.label}
					</NavLink>
				))}
			</nav>
		</aside>
	);
};
