import { IconNames } from "../../components/Icon/Icon.types";

export interface SidebarProps {
	menu: IMenuItem[];
}

interface IMenuItem {
	id: UniqueId;
	icon: IconNames;
	link: string;
	label: string;
}
