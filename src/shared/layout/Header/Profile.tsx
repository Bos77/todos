import React, { useState } from "react";
import cn from "classnames";
import { useSignOut } from "presentation/Auth/hooks";

import { Avatar, Button, Icon } from "../../components";
import { useAuth } from "../../context";
import { useOutsideClick } from "../../hooks/useOutsideClick";

export const Profile: React.FC = () => {
	const auth = useAuth();
	const signOut = useSignOut();

	const [isMenuVisible, setIsMenuVisible] = useState<boolean>(false);

	const ref = useOutsideClick(() => {
		setIsMenuVisible(false);
	});

	return (
		<div
			className={cn("drop-down", {
				"drop-down_active": isMenuVisible,
			})}
			ref={ref as React.RefObject<HTMLDivElement>}
		>
			<Button
				variant="filled"
				size="lg"
				className="header__profile"
				onClick={() => {
					setIsMenuVisible((prev) => !prev);
				}}
			>
				<Avatar size="sm" className="mr-4" />
				{auth?.user?.name}
				<Icon icon="arrow-down" className="ml-4" />
			</Button>

			<div className="drop-down__menu">
				<Button
					variant="filled"
					size="md"
					className="log-out w-full font-bold"
					onClick={() => {
						signOut.mutate();
					}}
				>
					<Icon icon="logout" className="mr-4" />
					Log out
				</Button>
			</div>
		</div>
	);
};
