import React from "react";

import { Profile } from "./Profile";

import "./Header.scss";

export const Header = () => {
	return (
		<header className="header">
			<img
				src={require("assets/icons/header-logo.svg").default}
				alt="header-logo"
				className="header__img"
			/>

			<div className="flex items-center">
				<Profile />
			</div>
		</header>
	);
};
