export const constants = {
	KEY_TAB: "Tab",
	KEY_ESC: "Escape",
	HTTP_STATUS_NOT_AUTHORIZED: 401,
};
