import React, { createContext, ReactNode, useContext, useState } from "react";

import { UserModel } from "modules/User";

type AuthContextValue = {
	user: UserModel.IEntity | null;
	setUser: Function;
};
interface IProps {
	children: ReactNode;
}

const AuthContext = createContext<AuthContextValue | null>(null);

export const AuthProvider: React.FC<IProps> = ({ children }) => {
	const [user, setUser] = useState<UserModel.IEntity | null>(null);

	return <AuthContext.Provider value={{ user, setUser }}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
	return useContext(AuthContext);
};
