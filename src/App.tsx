import React, { useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { AnimatePresence } from "framer-motion";

import { initHttpClient } from "infrastructure";
import { constants } from "shared/constants";
import { useAuth } from "shared/context";
import { MainLayout } from "shared/layout";

import { SignIn } from "./presentation/Auth";
import { CheckinIdentity } from "./presentation/Auth/components";
import { useGetMe } from "./presentation/Auth/hooks";
import { ErrorBoundary } from "./presentation/pages";
import { TodosList } from "./presentation/Todo";
import { UserList } from "./presentation/User";

export const App = () => {
	const user = useGetMe();
	const auth = useAuth();
	const navigate = useNavigate();

	useEffect(() => {
		initHttpClient((error) => {
			if (error.response?.status === constants.HTTP_STATUS_NOT_AUTHORIZED) {
				auth?.setUser(null);
				navigate("/sign-in");
			}
		});
	}, []);

	return (
		<ErrorBoundary>
			<div className="page">
				<ToastContainer />

				<AnimatePresence>
					{user.isFetching ? (
						<CheckinIdentity />
					) : auth?.user ? (
						<Routes>
							<Route path="/" element={<MainLayout />}>
								<Route index={true} element={<TodosList />} />
								<Route path="/users" element={<UserList />} />
							</Route>
						</Routes>
					) : (
						<Routes>
							<Route path="/sign-in" element={<SignIn />} />
						</Routes>
					)}
				</AnimatePresence>
			</div>
		</ErrorBoundary>
	);
};
