import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import ReactDOM from "react-dom/client";
import { SkeletonTheme } from "react-loading-skeleton";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

import { AuthProvider } from "./shared/context";
import { App } from "./App";

import "assets/styles/all.scss";

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLDivElement);
const queryClient = new QueryClient({
	logger: {
		log: () => {},
		warn: () => {},
		error: () => {},
	},

	defaultOptions: {
		queries: {
			retry: false,
			refetchOnWindowFocus: false,
			refetchOnReconnect: false,
		},
	},
});
root.render(
	<Router>
		<QueryClientProvider client={queryClient}>
			<SkeletonTheme baseColor="#e6edf5" highlightColor="#ecf3ff" duration={1}>
				<AuthProvider>
					<App />
				</AuthProvider>
			</SkeletonTheme>

			{process.env.NODE_ENV === "development" && (
				<ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
			)}
		</QueryClientProvider>
	</Router>
);
