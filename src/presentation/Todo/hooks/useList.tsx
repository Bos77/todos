import { useQuery } from "@tanstack/react-query";

import { mapTodosListResponse, TodoModel, todoRepository } from "../../../modules/Todo";

export const useList = () => {
	return useQuery<unknown, unknown, TodoModel.IEntity[]>([TodoModel.LIST], async () => {
		const todos = await todoRepository.getTodos();
		return mapTodosListResponse(todos);
	});
};
