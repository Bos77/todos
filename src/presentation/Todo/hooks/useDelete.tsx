import { useMutation, useQueryClient } from "@tanstack/react-query";
import { get } from "lodash";

import { notifier } from "../../../infrastructure";
import { TodoModel, todoRepository } from "../../../modules/Todo";

export const useDelete = (id: number, onSuccess: Function) => {
	const queryClient = useQueryClient();

	return useMutation(() => todoRepository.deleteTodo(id), {
		onSuccess: () => {
			queryClient.invalidateQueries({ queryKey: [TodoModel.LIST] });
			onSuccess();
			notifier.success("Successfully deleted");
		},
		onError: (error) => {
			notifier.error(get(error, "response.data.message") ?? "Something went wrong");
		},
	});
};
