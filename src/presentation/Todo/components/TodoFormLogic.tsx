import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import { TodoFormSchema, TodoModel } from "modules/Todo";

import { TodoFormView } from "./TodoFormView";

interface ITodoFormLogicProps {
	defaultValues: TodoModel.ITodoForm | undefined;
	onSubmit: Function;
}

export const TodoFormLogic: React.FC<ITodoFormLogicProps> = ({ defaultValues, onSubmit }) => {
	const form = useForm<TodoModel.ITodoForm>({
		values: defaultValues,
		resolver: yupResolver(TodoFormSchema),
	});

	const handleSubmit = async (todo: TodoModel.ITodoForm) => {
		await onSubmit(todo)
			.catch(() => {})
			.finally(() => form.reset(todo));
	};

	return <TodoFormView form={form} onSubmit={handleSubmit} />;
};
