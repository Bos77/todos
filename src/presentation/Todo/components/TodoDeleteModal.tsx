import React from "react";

import { Button, Modal, Title } from "../../../shared/components";
import { useDelete } from "../hooks";

interface ITodoDeleteModalProps {
	isOpen: boolean;
	id: number;
	onClose: Function;
}

export const TodoDeleteModal: React.FC<ITodoDeleteModalProps> = ({ isOpen, id, onClose }) => {
	const todoMutation = useDelete(id, onClose);

	return (
		<Modal isOpen={isOpen} onClose={onClose}>
			<Title as="h3" text="Are you sure you want to delete" className="mb-8 text-center" />

			<div className="flex justify-center">
				<Button
					variant="outlined"
					size="lg"
					text="Cancel"
					className="mr-4"
					onClick={() => {
						onClose();
					}}
				/>
				<Button
					variant="primary"
					size="lg"
					text="Delete"
					onClick={() => {
						todoMutation.mutate();
					}}
					isLoading={todoMutation.isLoading}
				/>
			</div>
		</Modal>
	);
};
