import React from "react";

import { AppModal } from "../../../shared/components";

import { TodoFormApi } from "./TodoFormApi";

interface ITodoModalProps {
	isOpen: boolean;
	onClose: React.MouseEventHandler;
	todoId: number | null;
}

export const TodoModal: React.FC<ITodoModalProps> = ({ isOpen, onClose, todoId }) => {
	return (
		<AppModal isOpen={isOpen} title={todoId ? "Update todo" : "Create todo"} onClose={onClose}>
			<TodoFormApi id={todoId} onClose={onClose} />
		</AppModal>
	);
};
