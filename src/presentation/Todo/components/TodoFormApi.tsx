import React from "react";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { get } from "lodash";

import { notifier } from "../../../infrastructure";
import { mapTodoResponse, TodoModel, todoRepository } from "../../../modules/Todo";

import { TodoFormLogic } from "./TodoFormLogic";

interface ITodoFormApi {
	id: number | null;
	onClose: Function;
}

export const TodoFormApi: React.FC<ITodoFormApi> = ({ id, onClose }) => {
	const queryClient = useQueryClient();

	const todoSingle = useQuery(
		[TodoModel.SINGLE, id],
		async () => {
			const todo = await todoRepository.getTodo(id as number);
			return mapTodoResponse(todo);
		},
		{
			enabled: !!id,
		}
	);

	const todoMutation = useMutation<unknown, unknown, TodoModel.ITodoForm>(
		(todo) => (id ? todoRepository.updateTodo(id, todo) : todoRepository.createTodo(todo)),
		{
			onSuccess: () => {
				queryClient.invalidateQueries({ queryKey: [TodoModel.LIST] });
				onClose();
				notifier.success("Successfully done");
			},
			onError: (error) => {
				notifier.error(get(error, "response.data.message") ?? "Something went wrong");
			},
		}
	);

	if (todoSingle.isFetching)
		return (
			<div className="flex items-center justify-center p-5">
				<span className="get-me-spinner__loader"></span>
			</div>
		);

	return <TodoFormLogic defaultValues={todoSingle.data} onSubmit={todoMutation.mutateAsync} />;
};
