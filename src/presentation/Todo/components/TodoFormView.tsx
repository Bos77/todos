import React from "react";
import { UseFormReturn } from "react-hook-form";

import { TodoModel } from "../../../modules/Todo";
import { Button, Input } from "../../../shared/components";
import { Textarea } from "../../../shared/components/Textarea";

interface ITodoFormViewProps {
	form: UseFormReturn<TodoModel.ITodoForm>;
	onSubmit: (data: TodoModel.ITodoForm) => any;
}

export const TodoFormView: React.FC<ITodoFormViewProps> = ({ form, onSubmit }) => {
	const { formState, register, handleSubmit } = form;
	const { errors, isSubmitting } = formState;

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<Input
				{...register("title")}
				size="lg"
				variant="outlined"
				label="Title"
				placeholder="Do home assignment"
				errorMessage={errors?.title?.message}
				outerClass="mb-6"
				type="text"
			/>

			<Textarea
				{...register("description")}
				variant="outlined"
				label="Description"
				placeholder="Comment"
				errorMessage={errors?.description?.message}
				outerClass="mb-10"
			/>

			<Button
				variant="primary"
				size="lg"
				text="Submit"
				className="mr-0 ml-auto"
				isLoading={isSubmitting}
			/>
		</form>
	);
};
