import React, { useState } from "react";
import { motion } from "framer-motion";

import { ActionButtons, PageHeading, Table } from "shared/components";
import { useAuth } from "shared/context";
import { TodoModel } from "modules/Todo";
import { UserModel } from "modules/User";

import { TodoDeleteModal } from "./components/TodoDeleteModal";
import { TodoModal } from "./components/TodoModal";
import { useList } from "./hooks";

export const List = () => {
	const todos = useList();
	const auth = useAuth();
	const [todoId, setTodoId] = useState<number | null>(null);

	const [confirmModal, setConfirmModal] = useState<boolean>(false);
	const [isOpen, setIsOpen] = useState<boolean>(false);

	return (
		<motion.div
			transition={{ duration: 0.5, stiffness: 10, damping: 20 }}
			initial={{ opacity: 0, translateY: "20px" }}
			animate={{ opacity: 1, translateY: 0 }}
			exit={{ opacity: 0, translateY: 0 }}
		>
			<TodoModal
				isOpen={isOpen}
				onClose={() => {
					setIsOpen(false);
					setTodoId(null);
				}}
				todoId={todoId}
			/>

			<TodoDeleteModal
				id={todoId as number}
				isOpen={confirmModal}
				onClose={() => {
					setConfirmModal(false);
				}}
			/>

			<PageHeading
				title="Todos"
				btnText="Add Todos"
				onBtnClick={() => setIsOpen((prev) => !prev)}
			/>

			<Table<TodoModel.IEntity>
				rowKey="id"
				emptyUiText="There are no todos yet"
				isLoading={todos.isFetching}
				items={todos.data || []}
				columns={[
					{
						title: "ID",
						dataKey: "id",
						render: (value) => value,
					},
					{
						title: "Title",
						dataKey: "title",
						render: (value) => value,
					},
					{
						title: "Description",
						dataKey: "description",
						render: (value) => value,
					},
					{
						title: "Owner",
						dataKey: "createdBy",
						render: (value) => value,
					},
					{
						title: "",
						dataKey: "id",
						render: (value, values) =>
							(auth?.user?.role === UserModel.Roles.admin ||
								values.createdBy === auth?.user?.role) && (
								<ActionButtons
									onEdit={() => {
										setTodoId(value);
										setIsOpen(true);
									}}
									onDelete={() => {
										setTodoId(value);
										setConfirmModal(true);
									}}
								/>
							),
					},
				]}
			/>
		</motion.div>
	);
};
