import React, { Component, ReactNode } from "react";

import { Icon, Title } from "../../shared/components";

interface IProps {
	children?: ReactNode;
}

interface IState {
	hasError: boolean;
}

export class ErrorBoundary extends Component<IProps, IState> {
	public state: IState = {
		hasError: false,
	};

	public static getDerivedStateFromError(_: Error): IState {
		return { hasError: true };
	}

	public render() {
		if (this.state.hasError) {
			return (
				<div className="error-page">
					<Icon icon="error" className="mb-10" />
					<Title as="h2" text="Something went wrong" />
				</div>
			);
		}

		return this.props.children;
	}
}
