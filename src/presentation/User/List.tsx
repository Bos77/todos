import React from "react";
import { motion } from "framer-motion";

import { PageHeading } from "shared/components";

import { UserCard } from "../../shared/components/Card";
import { UserCardSkeleton } from "../../shared/components/Card/UserCardSkeleton";

import { useList } from "./hooks";

export const List = () => {
	const users = useList();

	return (
		<motion.div
			transition={{ duration: 0.5, stiffness: 10, damping: 20 }}
			initial={{ opacity: 0, translateY: "20px" }}
			animate={{ opacity: 1, translateY: 0 }}
			exit={{ opacity: 0, translateY: 0 }}
		>
			<PageHeading title="Users" />

			<div className="grid grid-cols-4 gap-6">
				{users.isFetching
					? Array(2)
							.fill(1)
							.map((_, index) => <UserCardSkeleton key={index} />)
					: users.data?.map((user) => (
							<UserCard key={user.name} name={user.name} role={user.role} />
					  ))}
			</div>
		</motion.div>
	);
};
