import { useQuery } from "@tanstack/react-query";

import { mapUserListResponse, UserModel, userRepository } from "../../../modules/User";

export const useList = () => {
	return useQuery<unknown, unknown, UserModel.IEntity[]>([UserModel.LIST], async () => {
		const users = await userRepository.getUsers();
		return mapUserListResponse(users);
	});
};
