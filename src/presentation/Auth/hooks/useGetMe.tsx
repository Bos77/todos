import { useNavigate } from "react-router-dom";
import { useQuery } from "@tanstack/react-query";

import { useAuth } from "shared/context";
import { UserModel, userRepository } from "modules/User";

export const useGetMe = () => {
	const auth = useAuth();
	const navigate = useNavigate();

	return useQuery<unknown, unknown, UserModel.IEntity>(
		[UserModel.ME],
		async () => {
			return await userRepository.getCurrentCredentials();
		},
		{
			onSuccess: (user) => {
				auth?.setUser(user);
			},

			onError: () => {
				auth?.setUser(null);
				navigate("/sign-in");
			},
		}
	);
};
