import { useNavigate } from "react-router-dom";
import { useMutation } from "@tanstack/react-query";
import { get } from "lodash";

import { notifier } from "infrastructure";
import { useAuth } from "shared/context";
import { mapSignInResponse, UserModel, userRepository } from "modules/User";

export const useSignIn = () => {
	const auth = useAuth();
	const navigate = useNavigate();

	return useMutation<UserModel.IEntity, unknown, UserModel.ISignInForm, unknown>({
		mutationFn: async (userCredentials) => {
			const userEntity = await userRepository.signIn(userCredentials);
			return mapSignInResponse(userEntity);
		},
		onSuccess: (user) => {
			auth?.setUser(user);
			navigate("/");
		},
		onError: (error) => {
			auth?.setUser(null);
			notifier.error(get(error, "response.data.message") ?? "");
		},
	});
};
