import { useNavigate } from "react-router-dom";
import { useMutation } from "@tanstack/react-query";

import { notifier } from "infrastructure";
import { useAuth } from "shared/context";
import { userRepository } from "modules/User";

export const useSignOut = () => {
	const auth = useAuth();
	const navigate = useNavigate();

	return useMutation({
		mutationFn: () => userRepository.signOut(),
		onSuccess: () => {
			auth?.setUser(null);
			navigate("/sign-in");
		},
		onError: () => notifier.error("Something went wrong"),
	});
};
