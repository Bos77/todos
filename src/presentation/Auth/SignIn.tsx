import React from "react";
import { motion } from "framer-motion";

import { Icon, Title } from "../../shared/components";

import { SignInFormLogic } from "./components";
import { useSignIn } from "./hooks";

import "./styles/SignIn.scss";

export const SignIn = () => {
	const singIn = useSignIn();

	return (
		<motion.div
			transition={{ duration: 0.5, stiffness: 10, damping: 20 }}
			initial={{ opacity: 0, translateY: "20px" }}
			animate={{ opacity: 1, translateY: 0 }}
			exit={{ opacity: 0, translateY: 0 }}
			className="sign-in__wrapper flex w-full items-start"
		>
			<div className="sign-in__hero">
				<div className="mb-8 flex items-center">
					<Icon icon="logo" className="mr-4" />
					<Title as="h4" text="Todoist" className="text-white" />
				</div>
				<Title
					as="h1"
					text="Your place to work
                    Plan. Create. Control."
					className="mb-16 text-white"
				/>
				<Icon icon="sign-in-hero" className="w-full" />
			</div>

			<div className="sign-in__form">
				<Title as="h3" text="Sign In to Todoist" className="mb-12 text-center" />

				<SignInFormLogic onSubmit={singIn.mutateAsync} />
			</div>
		</motion.div>
	);
};
