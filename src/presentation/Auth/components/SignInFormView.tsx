import React from "react";
import { UseFormReturn } from "react-hook-form";

import { Button, Input } from "shared/components";
import { UserModel } from "modules/User";

interface SignInFormViewProps {
	form: UseFormReturn<UserModel.ISignInForm>;
	onSubmit: (data: UserModel.ISignInForm) => any;
}

export const SignInFormView: React.FC<SignInFormViewProps> = ({ form, onSubmit }) => {
	const { formState, register, handleSubmit } = form;
	const { errors, isSubmitted } = formState;

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<Input
				size="lg"
				variant="outlined"
				label="Email Address"
				placeholder="youremail@gmail.com"
				outerClass="mb-8"
				type="text"
				errorMessage={errors?.login?.message}
				{...register("login")}
			/>
			<Input
				size="lg"
				variant="outlined"
				label="Password"
				placeholder="***"
				outerClass="mb-20"
				type="password"
				errorMessage={errors?.password?.message}
				{...register("password")}
			/>
			<Button
				variant="primary"
				size="lg"
				text="Sign In"
				className="mx-auto w-full justify-center"
				isLoading={isSubmitted}
			/>
		</form>
	);
};
