import React from "react";

import { Text } from "shared/components";

export const CheckinIdentity = () => {
	return (
		<div className="get-me-spinner">
			<span className="get-me-spinner__loader"></span>
			<Text as="p" text="Checking your Identity" weight="bold" size="x-lg" className="mt-6" />
		</div>
	);
};
