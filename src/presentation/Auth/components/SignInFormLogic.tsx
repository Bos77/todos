import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import { UserModel, UserSignInSchema } from "../../../modules/User";

import { SignInFormView } from "./SignInFormView";

interface SignInFormLogicProps {
	onSubmit: (data: UserModel.ISignInForm) => Promise<UserModel.IEntity>;
}

export const SignInFormLogic: React.FC<SignInFormLogicProps> = ({ onSubmit }) => {
	const form = useForm<UserModel.ISignInForm>({
		mode: "onSubmit",
		resolver: yupResolver(UserSignInSchema),
		defaultValues: {
			login: "",
			password: "",
		},
	});

	const handleSubmit = (userCredentials: UserModel.ISignInForm) => {
		onSubmit(userCredentials)
			.then(() => form.reset())
			.catch(() => {});
	};

	return <SignInFormView form={form} onSubmit={handleSubmit} />;
};
